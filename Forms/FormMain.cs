﻿using ItemTableReader.Forms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace ItemTableReader
{
    public partial class FormMain : Form
    {
        public FormMain()
        {
            InitializeComponent();
            // OpenFileDialog ofd = new OpenFileDialog();
            // ofd.Filter = "Item table (*.bit)|*.bit|All files (*.*)|*.*";
            // ofd.FilterIndex = 1;
            //  if (ofd.ShowDialog() != DialogResult.Yes) Application.Exit();
            Stream str = File.Open(System.IO.Path.GetFullPath(@"..\..\") + @"\item_table.bit",FileMode.Open);
            header.Load(str);
        }

        ItemTableHeader header = new ItemTableHeader();

        private void btnWep_Click(object sender, EventArgs e)
        {
            new FormWeapons().Show();
        }

        private void btnCloth_Click(object sender, EventArgs e)
        {
            new FormClothes().Show();
        }

        private void btnElixirs_Click(object sender, EventArgs e)
        {
            new FormElixirs().Show();
        }

        private void btnAccessories_Click(object sender, EventArgs e)
        {
            new FormAccessories().Show();
        }

        private void btnPasses_Click(object sender, EventArgs e)
        {
            new FormPasses().Show();
        }

        private void btnBooks_Click(object sender, EventArgs e)
        {
            new FormBooks().Show();
        }

        private void btnResources_Click(object sender, EventArgs e)
        {
            new FormResources().Show();
        }

        private void btnFindItem_Click(object sender, EventArgs e)
        {
            try
            {
                ushort itrank = Convert.ToUInt16(textBoxFindItemRank.Text);
                string items = "";
                if (ItemParser.ItemRanks.ContainsKey(itrank))
                {
                    foreach (ItemBase b in ItemParser.ItemRanks[itrank]) items += b.FullName + '\n';
                }
                else items = "Not found";
                MessageBox.Show(items);
            }
            catch { MessageBox.Show("Invalid Input"); }
        }

        private void btnAllRanks_Click(object sender, EventArgs e)
        {
            new FormItemRanks().Show();
        }

        private void btnLifes_Click(object sender, EventArgs e)
        {
            new FormLifes().Show();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            SaveFileDialog sfd = new SaveFileDialog();
            sfd.ShowDialog();
            header.Save(sfd.OpenFile());
        }

        private void btnPotions_Click(object sender, EventArgs e)
        {
            new FormPotion().Show(); 
        }
    }
}
