﻿namespace ItemTableReader
{
    partial class FormMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnWep = new System.Windows.Forms.Button();
            this.btnCloth = new System.Windows.Forms.Button();
            this.btnElixirs = new System.Windows.Forms.Button();
            this.btnAccessories = new System.Windows.Forms.Button();
            this.btnPasses = new System.Windows.Forms.Button();
            this.btnBooks = new System.Windows.Forms.Button();
            this.btnResources = new System.Windows.Forms.Button();
            this.textBoxFindItemRank = new System.Windows.Forms.TextBox();
            this.btnFindItem = new System.Windows.Forms.Button();
            this.btnAllRanks = new System.Windows.Forms.Button();
            this.btnLifes = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnPotions = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnSocket = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnWep
            // 
            this.btnWep.Location = new System.Drawing.Point(17, 16);
            this.btnWep.Margin = new System.Windows.Forms.Padding(4);
            this.btnWep.Name = "btnWep";
            this.btnWep.Size = new System.Drawing.Size(100, 28);
            this.btnWep.TabIndex = 0;
            this.btnWep.Text = "Weapons";
            this.btnWep.UseVisualStyleBackColor = true;
            this.btnWep.Click += new System.EventHandler(this.btnWep_Click);
            // 
            // btnCloth
            // 
            this.btnCloth.Location = new System.Drawing.Point(17, 53);
            this.btnCloth.Margin = new System.Windows.Forms.Padding(4);
            this.btnCloth.Name = "btnCloth";
            this.btnCloth.Size = new System.Drawing.Size(100, 28);
            this.btnCloth.TabIndex = 1;
            this.btnCloth.Text = "Clothes";
            this.btnCloth.UseVisualStyleBackColor = true;
            this.btnCloth.Click += new System.EventHandler(this.btnCloth_Click);
            // 
            // btnElixirs
            // 
            this.btnElixirs.Location = new System.Drawing.Point(17, 90);
            this.btnElixirs.Margin = new System.Windows.Forms.Padding(4);
            this.btnElixirs.Name = "btnElixirs";
            this.btnElixirs.Size = new System.Drawing.Size(100, 28);
            this.btnElixirs.TabIndex = 2;
            this.btnElixirs.Text = "Elixirs";
            this.btnElixirs.UseVisualStyleBackColor = true;
            this.btnElixirs.Click += new System.EventHandler(this.btnElixirs_Click);
            // 
            // btnAccessories
            // 
            this.btnAccessories.Location = new System.Drawing.Point(17, 127);
            this.btnAccessories.Margin = new System.Windows.Forms.Padding(4);
            this.btnAccessories.Name = "btnAccessories";
            this.btnAccessories.Size = new System.Drawing.Size(100, 28);
            this.btnAccessories.TabIndex = 3;
            this.btnAccessories.Text = "Accessories";
            this.btnAccessories.UseVisualStyleBackColor = true;
            this.btnAccessories.Click += new System.EventHandler(this.btnAccessories_Click);
            // 
            // btnPasses
            // 
            this.btnPasses.Enabled = false;
            this.btnPasses.Location = new System.Drawing.Point(125, 163);
            this.btnPasses.Margin = new System.Windows.Forms.Padding(4);
            this.btnPasses.Name = "btnPasses";
            this.btnPasses.Size = new System.Drawing.Size(100, 28);
            this.btnPasses.TabIndex = 4;
            this.btnPasses.Text = "Passes";
            this.btnPasses.UseVisualStyleBackColor = true;
            this.btnPasses.Click += new System.EventHandler(this.btnPasses_Click);
            // 
            // btnBooks
            // 
            this.btnBooks.Location = new System.Drawing.Point(125, 16);
            this.btnBooks.Margin = new System.Windows.Forms.Padding(4);
            this.btnBooks.Name = "btnBooks";
            this.btnBooks.Size = new System.Drawing.Size(100, 28);
            this.btnBooks.TabIndex = 5;
            this.btnBooks.Text = "Books";
            this.btnBooks.UseVisualStyleBackColor = true;
            this.btnBooks.Click += new System.EventHandler(this.btnBooks_Click);
            // 
            // btnResources
            // 
            this.btnResources.Location = new System.Drawing.Point(125, 56);
            this.btnResources.Margin = new System.Windows.Forms.Padding(4);
            this.btnResources.Name = "btnResources";
            this.btnResources.Size = new System.Drawing.Size(100, 28);
            this.btnResources.TabIndex = 6;
            this.btnResources.Text = "Resources";
            this.btnResources.UseVisualStyleBackColor = true;
            this.btnResources.Click += new System.EventHandler(this.btnResources_Click);
            // 
            // textBoxFindItemRank
            // 
            this.textBoxFindItemRank.Location = new System.Drawing.Point(7, 80);
            this.textBoxFindItemRank.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxFindItemRank.Name = "textBoxFindItemRank";
            this.textBoxFindItemRank.Size = new System.Drawing.Size(96, 22);
            this.textBoxFindItemRank.TabIndex = 7;
            // 
            // btnFindItem
            // 
            this.btnFindItem.Location = new System.Drawing.Point(7, 114);
            this.btnFindItem.Margin = new System.Windows.Forms.Padding(4);
            this.btnFindItem.Name = "btnFindItem";
            this.btnFindItem.Size = new System.Drawing.Size(96, 28);
            this.btnFindItem.TabIndex = 8;
            this.btnFindItem.Text = "Find item rank";
            this.btnFindItem.UseVisualStyleBackColor = true;
            this.btnFindItem.Click += new System.EventHandler(this.btnFindItem_Click);
            // 
            // btnAllRanks
            // 
            this.btnAllRanks.Location = new System.Drawing.Point(7, 22);
            this.btnAllRanks.Margin = new System.Windows.Forms.Padding(4);
            this.btnAllRanks.Name = "btnAllRanks";
            this.btnAllRanks.Size = new System.Drawing.Size(82, 28);
            this.btnAllRanks.TabIndex = 9;
            this.btnAllRanks.Text = "All ranks";
            this.btnAllRanks.UseVisualStyleBackColor = true;
            this.btnAllRanks.Click += new System.EventHandler(this.btnAllRanks_Click);
            // 
            // btnLifes
            // 
            this.btnLifes.Location = new System.Drawing.Point(125, 93);
            this.btnLifes.Margin = new System.Windows.Forms.Padding(4);
            this.btnLifes.Name = "btnLifes";
            this.btnLifes.Size = new System.Drawing.Size(100, 28);
            this.btnLifes.TabIndex = 10;
            this.btnLifes.Text = "Lifes";
            this.btnLifes.UseVisualStyleBackColor = true;
            this.btnLifes.Click += new System.EventHandler(this.btnLifes_Click);
            // 
            // btnSave
            // 
            this.btnSave.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnSave.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSave.Location = new System.Drawing.Point(386, 0);
            this.btnSave.Margin = new System.Windows.Forms.Padding(4);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(86, 208);
            this.btnSave.TabIndex = 11;
            this.btnSave.Text = "Save Item Table";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnPotions
            // 
            this.btnPotions.Location = new System.Drawing.Point(17, 163);
            this.btnPotions.Margin = new System.Windows.Forms.Padding(4);
            this.btnPotions.Name = "btnPotions";
            this.btnPotions.Size = new System.Drawing.Size(100, 28);
            this.btnPotions.TabIndex = 12;
            this.btnPotions.Text = "Potions";
            this.btnPotions.UseVisualStyleBackColor = true;
            this.btnPotions.Click += new System.EventHandler(this.btnPotions_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnAllRanks);
            this.groupBox1.Controls.Add(this.textBoxFindItemRank);
            this.groupBox1.Controls.Add(this.btnFindItem);
            this.groupBox1.Location = new System.Drawing.Point(243, 25);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(110, 153);
            this.groupBox1.TabIndex = 13;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Item Ranks";
            // 
            // btnSocket
            // 
            this.btnSocket.Enabled = false;
            this.btnSocket.Location = new System.Drawing.Point(125, 128);
            this.btnSocket.Name = "btnSocket";
            this.btnSocket.Size = new System.Drawing.Size(100, 28);
            this.btnSocket.TabIndex = 14;
            this.btnSocket.Text = "Socket";
            this.btnSocket.UseVisualStyleBackColor = true;
            // 
            // FormMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(472, 208);
            this.Controls.Add(this.btnSocket);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.btnPotions);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.btnLifes);
            this.Controls.Add(this.btnResources);
            this.Controls.Add(this.btnBooks);
            this.Controls.Add(this.btnPasses);
            this.Controls.Add(this.btnAccessories);
            this.Controls.Add(this.btnElixirs);
            this.Controls.Add(this.btnCloth);
            this.Controls.Add(this.btnWep);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormMain";
            this.ShowIcon = false;
            this.Text = "ZeroN 9D - Item Editor (dev by Adek)";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnWep;
        private System.Windows.Forms.Button btnCloth;
        private System.Windows.Forms.Button btnElixirs;
        private System.Windows.Forms.Button btnAccessories;
        private System.Windows.Forms.Button btnPasses;
        private System.Windows.Forms.Button btnBooks;
        private System.Windows.Forms.Button btnResources;
        private System.Windows.Forms.TextBox textBoxFindItemRank;
        private System.Windows.Forms.Button btnFindItem;
        private System.Windows.Forms.Button btnAllRanks;
        private System.Windows.Forms.Button btnLifes;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnPotions;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnSocket;
    }
}