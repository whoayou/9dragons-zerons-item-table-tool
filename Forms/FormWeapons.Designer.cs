﻿namespace ItemTableReader
{
    partial class FormWeapons
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.comboBoxWep = new System.Windows.Forms.ComboBox();
            this.effectBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.itemWeaponBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.listBoxWeap = new System.Windows.Forms.ListBox();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.checkBoxCantNPC = new System.Windows.Forms.CheckBox();
            this.checkBoxCantTrade = new System.Windows.Forms.CheckBox();
            this.checkBoxCantStorage = new System.Windows.Forms.CheckBox();
            this.checkBoxStatsTrade = new System.Windows.Forms.CheckBox();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.iDDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.descriptionDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.valueDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabPageEffects = new System.Windows.Forms.TabPage();
            this.listBoxEffects = new System.Windows.Forms.ListBox();
            this.tabPageUnknownBytes = new System.Windows.Forms.TabPage();
            this.byteViewer = new System.ComponentModel.Design.ByteViewer();
            this.tabRequirements = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.labelMainStat = new System.Windows.Forms.Label();
            this.labelSecondaryStat = new System.Windows.Forms.Label();
            this.textBoxSecondaryStat = new System.Windows.Forms.TextBox();
            this.textBoxMainStat = new System.Windows.Forms.TextBox();
            this.labelLevel = new System.Windows.Forms.Label();
            this.textBoxLevel = new System.Windows.Forms.TextBox();
            this.labelTextLevel = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.tabDetails = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.labelMinDmg = new System.Windows.Forms.Label();
            this.labelMaxDmg = new System.Windows.Forms.Label();
            this.labelDurability = new System.Windows.Forms.Label();
            this.labelBalance = new System.Windows.Forms.Label();
            this.labelAR = new System.Windows.Forms.Label();
            this.labelCrit = new System.Windows.Forms.Label();
            this.textBoxMinDmg = new System.Windows.Forms.TextBox();
            this.textBoxMaxDmg = new System.Windows.Forms.TextBox();
            this.textBoxDurability = new System.Windows.Forms.TextBox();
            this.textBoxBalance = new System.Windows.Forms.TextBox();
            this.textBoxAR = new System.Windows.Forms.TextBox();
            this.textBoxCrit = new System.Windows.Forms.TextBox();
            this.textBoxHardness = new System.Windows.Forms.TextBox();
            this.labelHardness = new System.Windows.Forms.Label();
            this.labelTears = new System.Windows.Forms.Label();
            this.textBoxTears = new System.Windows.Forms.TextBox();
            this.labelPrice = new System.Windows.Forms.Label();
            this.textBoxPrice = new System.Windows.Forms.TextBox();
            this.labelSockets = new System.Windows.Forms.Label();
            this.textBoxSockets = new System.Windows.Forms.TextBox();
            this.labelRank = new System.Windows.Forms.Label();
            this.textBoxRank = new System.Windows.Forms.TextBox();
            this.labelModel = new System.Windows.Forms.Label();
            this.labelIcon = new System.Windows.Forms.Label();
            this.textBoxModel = new System.Windows.Forms.TextBox();
            this.textBoxIcon = new System.Windows.Forms.TextBox();
            this.labelTime = new System.Windows.Forms.Label();
            this.textBoxTime = new System.Windows.Forms.TextBox();
            this.labelContribClan = new System.Windows.Forms.Label();
            this.labelContribOthers = new System.Windows.Forms.Label();
            this.textBoxContribClan = new System.Windows.Forms.TextBox();
            this.textBoxContribOthers = new System.Windows.Forms.TextBox();
            this.labelLvlRed = new System.Windows.Forms.Label();
            this.textBoxLvlRed = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.labelCashCheck = new System.Windows.Forms.Label();
            this.labelMaxSockets = new System.Windows.Forms.Label();
            this.textBoxMaxSlots = new System.Windows.Forms.TextBox();
            this.textBoxCashCheck = new System.Windows.Forms.TextBox();
            this.tabBasicInfo = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.labelName = new System.Windows.Forms.Label();
            this.labelWeaponType = new System.Windows.Forms.Label();
            this.textBoxName = new System.Windows.Forms.TextBox();
            this.comboBoxWeaponType = new System.Windows.Forms.ComboBox();
            this.comboBoxWeaponThirdType = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.buttonLoadIcon = new System.Windows.Forms.Button();
            this.labelIco = new System.Windows.Forms.Label();
            this.labelXSDindex = new System.Windows.Forms.Label();
            this.textBoxXSDindex = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.comboBoxNickname = new System.Windows.Forms.ComboBox();
            this.comboBoxAddTo = new System.Windows.Forms.ComboBox();
            this.buttonCopy = new System.Windows.Forms.Button();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            ((System.ComponentModel.ISupportInitialize)(this.effectBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.itemWeaponBindingSource)).BeginInit();
            this.tabPage2.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.tabPageEffects.SuspendLayout();
            this.tabPageUnknownBytes.SuspendLayout();
            this.tabRequirements.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.tabDetails.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.tabBasicInfo.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.SuspendLayout();
            // 
            // comboBoxWep
            // 
            this.comboBoxWep.Dock = System.Windows.Forms.DockStyle.Top;
            this.comboBoxWep.FormattingEnabled = true;
            this.comboBoxWep.Items.AddRange(new object[] {
            "Weapons",
            "Weapons2\t",
            "Weapons3"});
            this.comboBoxWep.Location = new System.Drawing.Point(0, 0);
            this.comboBoxWep.Margin = new System.Windows.Forms.Padding(4);
            this.comboBoxWep.Name = "comboBoxWep";
            this.comboBoxWep.Size = new System.Drawing.Size(1237, 24);
            this.comboBoxWep.TabIndex = 2;
            this.comboBoxWep.SelectedIndexChanged += new System.EventHandler(this.comboBoxWep_SelectedIndexChanged);
            // 
            // effectBindingSource
            // 
            this.effectBindingSource.DataSource = typeof(ItemTableReader.Effect);
            // 
            // itemWeaponBindingSource
            // 
            this.itemWeaponBindingSource.DataSource = typeof(ItemTableReader.ItemWeapon);
            // 
            // listBoxWeap
            // 
            this.listBoxWeap.FormattingEnabled = true;
            this.listBoxWeap.ItemHeight = 16;
            this.listBoxWeap.Location = new System.Drawing.Point(12, 48);
            this.listBoxWeap.Name = "listBoxWeap";
            this.listBoxWeap.Size = new System.Drawing.Size(309, 500);
            this.listBoxWeap.TabIndex = 3;
            this.listBoxWeap.SelectedIndexChanged += new System.EventHandler(this.listBoxWeap_SelectedIndexChanged_1);
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.tableLayoutPanel1);
            this.tabPage2.Location = new System.Drawing.Point(4, 25);
            this.tabPage2.Margin = new System.Windows.Forms.Padding(4);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(4);
            this.tabPage2.Size = new System.Drawing.Size(849, 465);
            this.tabPage2.TabIndex = 6;
            this.tabPage2.Text = "Additional Options";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.Controls.Add(this.checkBoxCantNPC, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.checkBoxCantTrade, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.checkBoxCantStorage, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.checkBoxStatsTrade, 0, 3);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(4, 4);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(4);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 4;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(841, 457);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // checkBoxCantNPC
            // 
            this.checkBoxCantNPC.AutoSize = true;
            this.checkBoxCantNPC.Location = new System.Drawing.Point(4, 4);
            this.checkBoxCantNPC.Margin = new System.Windows.Forms.Padding(4);
            this.checkBoxCantNPC.Name = "checkBoxCantNPC";
            this.checkBoxCantNPC.Size = new System.Drawing.Size(173, 21);
            this.checkBoxCantNPC.TabIndex = 0;
            this.checkBoxCantNPC.Text = "Cannot be sold at NPC";
            this.checkBoxCantNPC.UseVisualStyleBackColor = true;
            this.checkBoxCantNPC.CheckedChanged += new System.EventHandler(this.checkBoxCantNPC_CheckedChanged);
            // 
            // checkBoxCantTrade
            // 
            this.checkBoxCantTrade.AutoSize = true;
            this.checkBoxCantTrade.Location = new System.Drawing.Point(4, 62);
            this.checkBoxCantTrade.Margin = new System.Windows.Forms.Padding(4);
            this.checkBoxCantTrade.Name = "checkBoxCantTrade";
            this.checkBoxCantTrade.Size = new System.Drawing.Size(247, 21);
            this.checkBoxCantTrade.TabIndex = 1;
            this.checkBoxCantTrade.Text = "Cannot be traded between players";
            this.checkBoxCantTrade.UseVisualStyleBackColor = true;
            this.checkBoxCantTrade.CheckedChanged += new System.EventHandler(this.checkBoxCantTrade_CheckedChanged);
            // 
            // checkBoxCantStorage
            // 
            this.checkBoxCantStorage.AutoSize = true;
            this.checkBoxCantStorage.Location = new System.Drawing.Point(4, 33);
            this.checkBoxCantStorage.Margin = new System.Windows.Forms.Padding(4);
            this.checkBoxCantStorage.Name = "checkBoxCantStorage";
            this.checkBoxCantStorage.Size = new System.Drawing.Size(181, 21);
            this.checkBoxCantStorage.TabIndex = 2;
            this.checkBoxCantStorage.Text = "Cannot move to storage";
            this.checkBoxCantStorage.UseVisualStyleBackColor = true;
            this.checkBoxCantStorage.CheckedChanged += new System.EventHandler(this.checkBoxCantStorage_CheckedChanged);
            // 
            // checkBoxStatsTrade
            // 
            this.checkBoxStatsTrade.AutoSize = true;
            this.checkBoxStatsTrade.Location = new System.Drawing.Point(4, 91);
            this.checkBoxStatsTrade.Margin = new System.Windows.Forms.Padding(4);
            this.checkBoxStatsTrade.Name = "checkBoxStatsTrade";
            this.checkBoxStatsTrade.Size = new System.Drawing.Size(196, 21);
            this.checkBoxStatsTrade.TabIndex = 3;
            this.checkBoxStatsTrade.Text = "Stats can be traded (0/30)";
            this.checkBoxStatsTrade.UseVisualStyleBackColor = true;
            this.checkBoxStatsTrade.CheckedChanged += new System.EventHandler(this.checkBoxStatsTrade_CheckedChanged);
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.dataGridView1);
            this.tabPage1.Location = new System.Drawing.Point(4, 25);
            this.tabPage1.Margin = new System.Windows.Forms.Padding(4);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(4);
            this.tabPage1.Size = new System.Drawing.Size(849, 465);
            this.tabPage1.TabIndex = 5;
            this.tabPage1.Text = "Eff Edit";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AutoGenerateColumns = false;
            this.dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.iDDataGridViewTextBoxColumn,
            this.nameDataGridViewTextBoxColumn,
            this.descriptionDataGridViewTextBoxColumn,
            this.valueDataGridViewTextBoxColumn});
            this.dataGridView1.DataSource = this.effectBindingSource;
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView1.Location = new System.Drawing.Point(4, 4);
            this.dataGridView1.Margin = new System.Windows.Forms.Padding(4);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(841, 457);
            this.dataGridView1.TabIndex = 0;
            this.dataGridView1.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellValueChanged);
            // 
            // iDDataGridViewTextBoxColumn
            // 
            this.iDDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.iDDataGridViewTextBoxColumn.DataPropertyName = "ID";
            this.iDDataGridViewTextBoxColumn.HeaderText = "ID";
            this.iDDataGridViewTextBoxColumn.Name = "iDDataGridViewTextBoxColumn";
            this.iDDataGridViewTextBoxColumn.Width = 50;
            // 
            // nameDataGridViewTextBoxColumn
            // 
            this.nameDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.nameDataGridViewTextBoxColumn.DataPropertyName = "Name";
            this.nameDataGridViewTextBoxColumn.HeaderText = "Name";
            this.nameDataGridViewTextBoxColumn.Name = "nameDataGridViewTextBoxColumn";
            this.nameDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.nameDataGridViewTextBoxColumn.Width = 74;
            // 
            // descriptionDataGridViewTextBoxColumn
            // 
            this.descriptionDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.descriptionDataGridViewTextBoxColumn.DataPropertyName = "Description";
            this.descriptionDataGridViewTextBoxColumn.HeaderText = "Description";
            this.descriptionDataGridViewTextBoxColumn.Name = "descriptionDataGridViewTextBoxColumn";
            this.descriptionDataGridViewTextBoxColumn.ReadOnly = true;
            this.descriptionDataGridViewTextBoxColumn.Width = 108;
            // 
            // valueDataGridViewTextBoxColumn
            // 
            this.valueDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.valueDataGridViewTextBoxColumn.DataPropertyName = "Value";
            this.valueDataGridViewTextBoxColumn.HeaderText = "Value";
            this.valueDataGridViewTextBoxColumn.Name = "valueDataGridViewTextBoxColumn";
            this.valueDataGridViewTextBoxColumn.Width = 73;
            // 
            // tabPageEffects
            // 
            this.tabPageEffects.Controls.Add(this.listBoxEffects);
            this.tabPageEffects.Location = new System.Drawing.Point(4, 25);
            this.tabPageEffects.Margin = new System.Windows.Forms.Padding(4);
            this.tabPageEffects.Name = "tabPageEffects";
            this.tabPageEffects.Padding = new System.Windows.Forms.Padding(4);
            this.tabPageEffects.Size = new System.Drawing.Size(849, 465);
            this.tabPageEffects.TabIndex = 4;
            this.tabPageEffects.Text = "Eff Display";
            this.tabPageEffects.UseVisualStyleBackColor = true;
            // 
            // listBoxEffects
            // 
            this.listBoxEffects.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listBoxEffects.FormattingEnabled = true;
            this.listBoxEffects.ItemHeight = 16;
            this.listBoxEffects.Location = new System.Drawing.Point(4, 4);
            this.listBoxEffects.Margin = new System.Windows.Forms.Padding(4);
            this.listBoxEffects.Name = "listBoxEffects";
            this.listBoxEffects.Size = new System.Drawing.Size(841, 457);
            this.listBoxEffects.TabIndex = 0;
            // 
            // tabPageUnknownBytes
            // 
            this.tabPageUnknownBytes.Controls.Add(this.byteViewer);
            this.tabPageUnknownBytes.Location = new System.Drawing.Point(4, 25);
            this.tabPageUnknownBytes.Margin = new System.Windows.Forms.Padding(4);
            this.tabPageUnknownBytes.Name = "tabPageUnknownBytes";
            this.tabPageUnknownBytes.Padding = new System.Windows.Forms.Padding(4);
            this.tabPageUnknownBytes.Size = new System.Drawing.Size(849, 465);
            this.tabPageUnknownBytes.TabIndex = 3;
            this.tabPageUnknownBytes.Text = "Unknown bytes";
            this.tabPageUnknownBytes.UseVisualStyleBackColor = true;
            // 
            // byteViewer
            // 
            this.byteViewer.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Inset;
            this.byteViewer.ColumnCount = 1;
            this.byteViewer.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.byteViewer.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.byteViewer.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.byteViewer.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.byteViewer.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.byteViewer.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.byteViewer.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.byteViewer.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.byteViewer.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.byteViewer.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.byteViewer.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.byteViewer.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.byteViewer.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.byteViewer.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.byteViewer.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.byteViewer.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.byteViewer.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.byteViewer.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.byteViewer.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.byteViewer.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.byteViewer.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.byteViewer.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.byteViewer.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.byteViewer.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.byteViewer.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.byteViewer.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.byteViewer.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.byteViewer.Location = new System.Drawing.Point(0, 0);
            this.byteViewer.Margin = new System.Windows.Forms.Padding(4);
            this.byteViewer.Name = "byteViewer";
            this.byteViewer.RowCount = 1;
            this.byteViewer.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.byteViewer.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.byteViewer.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.byteViewer.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.byteViewer.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.byteViewer.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.byteViewer.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.byteViewer.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.byteViewer.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.byteViewer.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.byteViewer.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.byteViewer.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.byteViewer.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.byteViewer.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.byteViewer.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.byteViewer.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.byteViewer.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.byteViewer.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.byteViewer.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.byteViewer.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.byteViewer.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.byteViewer.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.byteViewer.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.byteViewer.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.byteViewer.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.byteViewer.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.byteViewer.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.byteViewer.Size = new System.Drawing.Size(638, 640);
            this.byteViewer.TabIndex = 0;
            // 
            // tabRequirements
            // 
            this.tabRequirements.Controls.Add(this.tableLayoutPanel3);
            this.tabRequirements.Location = new System.Drawing.Point(4, 25);
            this.tabRequirements.Margin = new System.Windows.Forms.Padding(4);
            this.tabRequirements.Name = "tabRequirements";
            this.tabRequirements.Padding = new System.Windows.Forms.Padding(4);
            this.tabRequirements.Size = new System.Drawing.Size(849, 465);
            this.tabRequirements.TabIndex = 1;
            this.tabRequirements.Text = "Requirements";
            this.tabRequirements.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 3;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel3.Controls.Add(this.labelMainStat, 0, 1);
            this.tableLayoutPanel3.Controls.Add(this.labelSecondaryStat, 0, 2);
            this.tableLayoutPanel3.Controls.Add(this.textBoxSecondaryStat, 1, 2);
            this.tableLayoutPanel3.Controls.Add(this.textBoxMainStat, 1, 1);
            this.tableLayoutPanel3.Controls.Add(this.labelLevel, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.textBoxLevel, 1, 0);
            this.tableLayoutPanel3.Controls.Add(this.labelTextLevel, 2, 0);
            this.tableLayoutPanel3.Controls.Add(this.textBox1, 2, 3);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(4, 4);
            this.tableLayoutPanel3.Margin = new System.Windows.Forms.Padding(4);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 4;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel3.Size = new System.Drawing.Size(841, 457);
            this.tableLayoutPanel3.TabIndex = 9;
            // 
            // labelMainStat
            // 
            this.labelMainStat.AutoSize = true;
            this.labelMainStat.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelMainStat.Location = new System.Drawing.Point(4, 30);
            this.labelMainStat.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelMainStat.Name = "labelMainStat";
            this.labelMainStat.Size = new System.Drawing.Size(83, 30);
            this.labelMainStat.TabIndex = 1;
            this.labelMainStat.Text = "Main stat";
            this.labelMainStat.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelSecondaryStat
            // 
            this.labelSecondaryStat.AutoSize = true;
            this.labelSecondaryStat.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelSecondaryStat.Location = new System.Drawing.Point(4, 60);
            this.labelSecondaryStat.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelSecondaryStat.Name = "labelSecondaryStat";
            this.labelSecondaryStat.Size = new System.Drawing.Size(83, 30);
            this.labelSecondaryStat.TabIndex = 3;
            this.labelSecondaryStat.Text = "Second stat";
            this.labelSecondaryStat.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // textBoxSecondaryStat
            // 
            this.textBoxSecondaryStat.Location = new System.Drawing.Point(95, 64);
            this.textBoxSecondaryStat.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxSecondaryStat.Name = "textBoxSecondaryStat";
            this.textBoxSecondaryStat.Size = new System.Drawing.Size(101, 22);
            this.textBoxSecondaryStat.TabIndex = 8;
            this.textBoxSecondaryStat.TextChanged += new System.EventHandler(this.textBoxSecondaryStat_TextChanged);
            // 
            // textBoxMainStat
            // 
            this.textBoxMainStat.Location = new System.Drawing.Point(95, 34);
            this.textBoxMainStat.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxMainStat.Name = "textBoxMainStat";
            this.textBoxMainStat.Size = new System.Drawing.Size(101, 22);
            this.textBoxMainStat.TabIndex = 7;
            this.textBoxMainStat.TextChanged += new System.EventHandler(this.textBoxMainStat_TextChanged);
            // 
            // labelLevel
            // 
            this.labelLevel.AutoSize = true;
            this.labelLevel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelLevel.Location = new System.Drawing.Point(4, 0);
            this.labelLevel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelLevel.Name = "labelLevel";
            this.labelLevel.Size = new System.Drawing.Size(83, 30);
            this.labelLevel.TabIndex = 9;
            this.labelLevel.Text = "Level";
            this.labelLevel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // textBoxLevel
            // 
            this.textBoxLevel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBoxLevel.Location = new System.Drawing.Point(95, 4);
            this.textBoxLevel.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxLevel.Name = "textBoxLevel";
            this.textBoxLevel.Size = new System.Drawing.Size(101, 22);
            this.textBoxLevel.TabIndex = 10;
            this.textBoxLevel.TextChanged += new System.EventHandler(this.textBoxLevel_TextChanged);
            // 
            // labelTextLevel
            // 
            this.labelTextLevel.AutoSize = true;
            this.labelTextLevel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelTextLevel.Location = new System.Drawing.Point(204, 0);
            this.labelTextLevel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelTextLevel.Name = "labelTextLevel";
            this.labelTextLevel.Size = new System.Drawing.Size(633, 30);
            this.labelTextLevel.TabIndex = 11;
            this.labelTextLevel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelTextLevel.TextChanged += new System.EventHandler(this.textBoxLevel_TextChanged);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(204, 94);
            this.textBox1.Margin = new System.Windows.Forms.Padding(4);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(240, 117);
            this.textBox1.TabIndex = 12;
            // 
            // tabDetails
            // 
            this.tabDetails.Controls.Add(this.tableLayoutPanel2);
            this.tabDetails.Location = new System.Drawing.Point(4, 25);
            this.tabDetails.Margin = new System.Windows.Forms.Padding(4);
            this.tabDetails.Name = "tabDetails";
            this.tabDetails.Padding = new System.Windows.Forms.Padding(4);
            this.tabDetails.Size = new System.Drawing.Size(849, 465);
            this.tabDetails.TabIndex = 0;
            this.tabDetails.Text = "Details";
            this.tabDetails.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 5;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 27F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 27F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 27F));
            this.tableLayoutPanel2.Controls.Add(this.labelMinDmg, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.labelMaxDmg, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.labelDurability, 0, 2);
            this.tableLayoutPanel2.Controls.Add(this.labelBalance, 0, 3);
            this.tableLayoutPanel2.Controls.Add(this.labelAR, 0, 4);
            this.tableLayoutPanel2.Controls.Add(this.labelCrit, 0, 5);
            this.tableLayoutPanel2.Controls.Add(this.textBoxMinDmg, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.textBoxMaxDmg, 1, 1);
            this.tableLayoutPanel2.Controls.Add(this.textBoxDurability, 1, 2);
            this.tableLayoutPanel2.Controls.Add(this.textBoxBalance, 1, 3);
            this.tableLayoutPanel2.Controls.Add(this.textBoxAR, 1, 4);
            this.tableLayoutPanel2.Controls.Add(this.textBoxCrit, 1, 5);
            this.tableLayoutPanel2.Controls.Add(this.textBoxHardness, 1, 7);
            this.tableLayoutPanel2.Controls.Add(this.labelHardness, 0, 7);
            this.tableLayoutPanel2.Controls.Add(this.labelTears, 0, 8);
            this.tableLayoutPanel2.Controls.Add(this.textBoxTears, 1, 8);
            this.tableLayoutPanel2.Controls.Add(this.labelPrice, 0, 10);
            this.tableLayoutPanel2.Controls.Add(this.textBoxPrice, 1, 10);
            this.tableLayoutPanel2.Controls.Add(this.labelSockets, 0, 11);
            this.tableLayoutPanel2.Controls.Add(this.textBoxSockets, 1, 11);
            this.tableLayoutPanel2.Controls.Add(this.labelRank, 3, 0);
            this.tableLayoutPanel2.Controls.Add(this.textBoxRank, 4, 0);
            this.tableLayoutPanel2.Controls.Add(this.labelModel, 3, 1);
            this.tableLayoutPanel2.Controls.Add(this.labelIcon, 3, 2);
            this.tableLayoutPanel2.Controls.Add(this.textBoxModel, 4, 1);
            this.tableLayoutPanel2.Controls.Add(this.textBoxIcon, 4, 2);
            this.tableLayoutPanel2.Controls.Add(this.labelTime, 3, 3);
            this.tableLayoutPanel2.Controls.Add(this.textBoxTime, 4, 3);
            this.tableLayoutPanel2.Controls.Add(this.labelContribClan, 3, 4);
            this.tableLayoutPanel2.Controls.Add(this.labelContribOthers, 3, 5);
            this.tableLayoutPanel2.Controls.Add(this.textBoxContribClan, 4, 4);
            this.tableLayoutPanel2.Controls.Add(this.textBoxContribOthers, 4, 5);
            this.tableLayoutPanel2.Controls.Add(this.labelLvlRed, 3, 7);
            this.tableLayoutPanel2.Controls.Add(this.textBoxLvlRed, 4, 7);
            this.tableLayoutPanel2.Controls.Add(this.label2, 3, 8);
            this.tableLayoutPanel2.Controls.Add(this.labelCashCheck, 3, 10);
            this.tableLayoutPanel2.Controls.Add(this.labelMaxSockets, 3, 11);
            this.tableLayoutPanel2.Controls.Add(this.textBoxMaxSlots, 4, 11);
            this.tableLayoutPanel2.Controls.Add(this.textBoxCashCheck, 4, 10);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(4, 4);
            this.tableLayoutPanel2.Margin = new System.Windows.Forms.Padding(4);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 13;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel2.Size = new System.Drawing.Size(841, 457);
            this.tableLayoutPanel2.TabIndex = 0;
            // 
            // labelMinDmg
            // 
            this.labelMinDmg.AutoSize = true;
            this.labelMinDmg.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelMinDmg.Location = new System.Drawing.Point(4, 0);
            this.labelMinDmg.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelMinDmg.Name = "labelMinDmg";
            this.labelMinDmg.Size = new System.Drawing.Size(84, 30);
            this.labelMinDmg.TabIndex = 7;
            this.labelMinDmg.Text = "Min dmg";
            this.labelMinDmg.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelMaxDmg
            // 
            this.labelMaxDmg.AutoSize = true;
            this.labelMaxDmg.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelMaxDmg.Location = new System.Drawing.Point(4, 30);
            this.labelMaxDmg.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelMaxDmg.Name = "labelMaxDmg";
            this.labelMaxDmg.Size = new System.Drawing.Size(84, 30);
            this.labelMaxDmg.TabIndex = 8;
            this.labelMaxDmg.Text = "Max dmg";
            this.labelMaxDmg.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelDurability
            // 
            this.labelDurability.AutoSize = true;
            this.labelDurability.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelDurability.Location = new System.Drawing.Point(4, 60);
            this.labelDurability.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelDurability.Name = "labelDurability";
            this.labelDurability.Size = new System.Drawing.Size(84, 30);
            this.labelDurability.TabIndex = 9;
            this.labelDurability.Text = "Durability";
            this.labelDurability.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelBalance
            // 
            this.labelBalance.AutoSize = true;
            this.labelBalance.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelBalance.Location = new System.Drawing.Point(4, 90);
            this.labelBalance.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelBalance.Name = "labelBalance";
            this.labelBalance.Size = new System.Drawing.Size(84, 30);
            this.labelBalance.TabIndex = 10;
            this.labelBalance.Text = "Balance";
            this.labelBalance.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelAR
            // 
            this.labelAR.AutoSize = true;
            this.labelAR.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelAR.Location = new System.Drawing.Point(4, 120);
            this.labelAR.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelAR.Name = "labelAR";
            this.labelAR.Size = new System.Drawing.Size(84, 30);
            this.labelAR.TabIndex = 11;
            this.labelAR.Text = "Attack Rate";
            this.labelAR.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelCrit
            // 
            this.labelCrit.AutoSize = true;
            this.labelCrit.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelCrit.Location = new System.Drawing.Point(4, 150);
            this.labelCrit.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelCrit.Name = "labelCrit";
            this.labelCrit.Size = new System.Drawing.Size(84, 30);
            this.labelCrit.TabIndex = 12;
            this.labelCrit.Text = "Critical Rate";
            this.labelCrit.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // textBoxMinDmg
            // 
            this.textBoxMinDmg.Location = new System.Drawing.Point(96, 4);
            this.textBoxMinDmg.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxMinDmg.Name = "textBoxMinDmg";
            this.textBoxMinDmg.Size = new System.Drawing.Size(132, 22);
            this.textBoxMinDmg.TabIndex = 13;
            this.textBoxMinDmg.Leave += new System.EventHandler(this.textBoxMinDmg_Leave);
            // 
            // textBoxMaxDmg
            // 
            this.textBoxMaxDmg.Location = new System.Drawing.Point(96, 34);
            this.textBoxMaxDmg.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxMaxDmg.Name = "textBoxMaxDmg";
            this.textBoxMaxDmg.Size = new System.Drawing.Size(132, 22);
            this.textBoxMaxDmg.TabIndex = 14;
            this.textBoxMaxDmg.TextChanged += new System.EventHandler(this.textBoxMaxDmg_TextChanged);
            // 
            // textBoxDurability
            // 
            this.textBoxDurability.Location = new System.Drawing.Point(96, 64);
            this.textBoxDurability.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxDurability.Name = "textBoxDurability";
            this.textBoxDurability.Size = new System.Drawing.Size(132, 22);
            this.textBoxDurability.TabIndex = 15;
            this.textBoxDurability.TextChanged += new System.EventHandler(this.textBoxDurability_TextChanged);
            // 
            // textBoxBalance
            // 
            this.textBoxBalance.Location = new System.Drawing.Point(96, 94);
            this.textBoxBalance.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxBalance.Name = "textBoxBalance";
            this.textBoxBalance.Size = new System.Drawing.Size(132, 22);
            this.textBoxBalance.TabIndex = 16;
            this.textBoxBalance.TextChanged += new System.EventHandler(this.textBoxBalance_TextChanged);
            // 
            // textBoxAR
            // 
            this.textBoxAR.Location = new System.Drawing.Point(96, 124);
            this.textBoxAR.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxAR.Name = "textBoxAR";
            this.textBoxAR.Size = new System.Drawing.Size(132, 22);
            this.textBoxAR.TabIndex = 17;
            this.textBoxAR.TextChanged += new System.EventHandler(this.textBoxAR_TextChanged);
            // 
            // textBoxCrit
            // 
            this.textBoxCrit.Location = new System.Drawing.Point(96, 154);
            this.textBoxCrit.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxCrit.Name = "textBoxCrit";
            this.textBoxCrit.Size = new System.Drawing.Size(132, 22);
            this.textBoxCrit.TabIndex = 18;
            this.textBoxCrit.TextChanged += new System.EventHandler(this.textBoxCrit_TextChanged);
            // 
            // textBoxHardness
            // 
            this.textBoxHardness.Location = new System.Drawing.Point(96, 184);
            this.textBoxHardness.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxHardness.Name = "textBoxHardness";
            this.textBoxHardness.Size = new System.Drawing.Size(132, 22);
            this.textBoxHardness.TabIndex = 19;
            this.textBoxHardness.TextChanged += new System.EventHandler(this.textBoxHardness_TextChanged);
            // 
            // labelHardness
            // 
            this.labelHardness.AutoSize = true;
            this.labelHardness.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelHardness.Location = new System.Drawing.Point(4, 180);
            this.labelHardness.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelHardness.Name = "labelHardness";
            this.labelHardness.Size = new System.Drawing.Size(84, 30);
            this.labelHardness.TabIndex = 20;
            this.labelHardness.Text = "Hardness";
            this.labelHardness.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelTears
            // 
            this.labelTears.AutoSize = true;
            this.labelTears.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelTears.Location = new System.Drawing.Point(4, 210);
            this.labelTears.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelTears.Name = "labelTears";
            this.labelTears.Size = new System.Drawing.Size(84, 30);
            this.labelTears.TabIndex = 21;
            this.labelTears.Text = "Tears";
            this.labelTears.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // textBoxTears
            // 
            this.textBoxTears.Location = new System.Drawing.Point(96, 214);
            this.textBoxTears.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxTears.Name = "textBoxTears";
            this.textBoxTears.Size = new System.Drawing.Size(132, 22);
            this.textBoxTears.TabIndex = 22;
            // 
            // labelPrice
            // 
            this.labelPrice.AutoSize = true;
            this.labelPrice.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelPrice.Location = new System.Drawing.Point(4, 240);
            this.labelPrice.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelPrice.Name = "labelPrice";
            this.labelPrice.Size = new System.Drawing.Size(84, 30);
            this.labelPrice.TabIndex = 23;
            this.labelPrice.Text = "Price";
            this.labelPrice.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // textBoxPrice
            // 
            this.textBoxPrice.Location = new System.Drawing.Point(96, 244);
            this.textBoxPrice.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxPrice.Name = "textBoxPrice";
            this.textBoxPrice.Size = new System.Drawing.Size(132, 22);
            this.textBoxPrice.TabIndex = 24;
            this.textBoxPrice.TextChanged += new System.EventHandler(this.textBoxPrice_TextChanged);
            // 
            // labelSockets
            // 
            this.labelSockets.AutoSize = true;
            this.labelSockets.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelSockets.Location = new System.Drawing.Point(4, 270);
            this.labelSockets.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelSockets.Name = "labelSockets";
            this.labelSockets.Size = new System.Drawing.Size(84, 30);
            this.labelSockets.TabIndex = 25;
            this.labelSockets.Text = "Sockets";
            this.labelSockets.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // textBoxSockets
            // 
            this.textBoxSockets.Location = new System.Drawing.Point(96, 274);
            this.textBoxSockets.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxSockets.Name = "textBoxSockets";
            this.textBoxSockets.Size = new System.Drawing.Size(132, 22);
            this.textBoxSockets.TabIndex = 26;
            this.textBoxSockets.TextChanged += new System.EventHandler(this.textBoxSockets_TextChanged);
            // 
            // labelRank
            // 
            this.labelRank.AutoSize = true;
            this.labelRank.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelRank.Location = new System.Drawing.Point(263, 0);
            this.labelRank.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelRank.Name = "labelRank";
            this.labelRank.Size = new System.Drawing.Size(149, 30);
            this.labelRank.TabIndex = 27;
            this.labelRank.Text = "Rank";
            this.labelRank.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // textBoxRank
            // 
            this.textBoxRank.Location = new System.Drawing.Point(420, 4);
            this.textBoxRank.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxRank.Name = "textBoxRank";
            this.textBoxRank.Size = new System.Drawing.Size(132, 22);
            this.textBoxRank.TabIndex = 28;
            this.textBoxRank.TextChanged += new System.EventHandler(this.textBoxRank_TextChanged);
            // 
            // labelModel
            // 
            this.labelModel.AutoSize = true;
            this.labelModel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelModel.Location = new System.Drawing.Point(263, 30);
            this.labelModel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelModel.Name = "labelModel";
            this.labelModel.Size = new System.Drawing.Size(149, 30);
            this.labelModel.TabIndex = 29;
            this.labelModel.Text = "Model index";
            this.labelModel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelIcon
            // 
            this.labelIcon.AutoSize = true;
            this.labelIcon.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelIcon.Location = new System.Drawing.Point(263, 60);
            this.labelIcon.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelIcon.Name = "labelIcon";
            this.labelIcon.Size = new System.Drawing.Size(149, 30);
            this.labelIcon.TabIndex = 30;
            this.labelIcon.Text = "Icon index";
            this.labelIcon.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // textBoxModel
            // 
            this.textBoxModel.Location = new System.Drawing.Point(420, 34);
            this.textBoxModel.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxModel.Name = "textBoxModel";
            this.textBoxModel.Size = new System.Drawing.Size(132, 22);
            this.textBoxModel.TabIndex = 31;
            this.textBoxModel.TextChanged += new System.EventHandler(this.textBoxModel_TextChanged);
            // 
            // textBoxIcon
            // 
            this.textBoxIcon.Location = new System.Drawing.Point(420, 64);
            this.textBoxIcon.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxIcon.Name = "textBoxIcon";
            this.textBoxIcon.Size = new System.Drawing.Size(132, 22);
            this.textBoxIcon.TabIndex = 32;
            this.textBoxIcon.TextChanged += new System.EventHandler(this.textBoxIcon_TextChanged);
            // 
            // labelTime
            // 
            this.labelTime.AutoSize = true;
            this.labelTime.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelTime.Location = new System.Drawing.Point(263, 90);
            this.labelTime.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelTime.Name = "labelTime";
            this.labelTime.Size = new System.Drawing.Size(149, 30);
            this.labelTime.TabIndex = 33;
            this.labelTime.Text = "Time";
            this.labelTime.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // textBoxTime
            // 
            this.textBoxTime.Location = new System.Drawing.Point(420, 94);
            this.textBoxTime.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxTime.Name = "textBoxTime";
            this.textBoxTime.Size = new System.Drawing.Size(132, 22);
            this.textBoxTime.TabIndex = 34;
            this.textBoxTime.TextChanged += new System.EventHandler(this.textBoxTime_TextChanged);
            // 
            // labelContribClan
            // 
            this.labelContribClan.AutoSize = true;
            this.labelContribClan.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelContribClan.Location = new System.Drawing.Point(263, 120);
            this.labelContribClan.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelContribClan.Name = "labelContribClan";
            this.labelContribClan.Size = new System.Drawing.Size(149, 30);
            this.labelContribClan.TabIndex = 35;
            this.labelContribClan.Text = "Clan contribution";
            this.labelContribClan.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelContribOthers
            // 
            this.labelContribOthers.AutoSize = true;
            this.labelContribOthers.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelContribOthers.Location = new System.Drawing.Point(263, 150);
            this.labelContribOthers.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelContribOthers.Name = "labelContribOthers";
            this.labelContribOthers.Size = new System.Drawing.Size(149, 30);
            this.labelContribOthers.TabIndex = 36;
            this.labelContribOthers.Text = "Contribution for others";
            this.labelContribOthers.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // textBoxContribClan
            // 
            this.textBoxContribClan.Location = new System.Drawing.Point(420, 124);
            this.textBoxContribClan.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxContribClan.Name = "textBoxContribClan";
            this.textBoxContribClan.Size = new System.Drawing.Size(132, 22);
            this.textBoxContribClan.TabIndex = 37;
            this.textBoxContribClan.TextChanged += new System.EventHandler(this.textBoxContribClan_TextChanged);
            // 
            // textBoxContribOthers
            // 
            this.textBoxContribOthers.Location = new System.Drawing.Point(420, 154);
            this.textBoxContribOthers.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxContribOthers.Name = "textBoxContribOthers";
            this.textBoxContribOthers.Size = new System.Drawing.Size(132, 22);
            this.textBoxContribOthers.TabIndex = 38;
            this.textBoxContribOthers.TextChanged += new System.EventHandler(this.textBoxContribOthers_TextChanged);
            // 
            // labelLvlRed
            // 
            this.labelLvlRed.AutoSize = true;
            this.labelLvlRed.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelLvlRed.Location = new System.Drawing.Point(263, 180);
            this.labelLvlRed.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelLvlRed.Name = "labelLvlRed";
            this.labelLvlRed.Size = new System.Drawing.Size(149, 30);
            this.labelLvlRed.TabIndex = 39;
            this.labelLvlRed.Text = "Level reducement";
            this.labelLvlRed.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // textBoxLvlRed
            // 
            this.textBoxLvlRed.Location = new System.Drawing.Point(420, 184);
            this.textBoxLvlRed.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxLvlRed.Name = "textBoxLvlRed";
            this.textBoxLvlRed.Size = new System.Drawing.Size(132, 22);
            this.textBoxLvlRed.TabIndex = 40;
            this.textBoxLvlRed.TextChanged += new System.EventHandler(this.textBoxLvlRed_TextChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(263, 210);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(46, 17);
            this.label2.TabIndex = 41;
            this.label2.Text = "label2";
            // 
            // labelCashCheck
            // 
            this.labelCashCheck.AutoSize = true;
            this.labelCashCheck.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelCashCheck.Location = new System.Drawing.Point(263, 240);
            this.labelCashCheck.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelCashCheck.Name = "labelCashCheck";
            this.labelCashCheck.Size = new System.Drawing.Size(149, 30);
            this.labelCashCheck.TabIndex = 42;
            this.labelCashCheck.Text = "Cash check (?)";
            this.labelCashCheck.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelMaxSockets
            // 
            this.labelMaxSockets.AutoSize = true;
            this.labelMaxSockets.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelMaxSockets.Location = new System.Drawing.Point(263, 270);
            this.labelMaxSockets.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelMaxSockets.Name = "labelMaxSockets";
            this.labelMaxSockets.Size = new System.Drawing.Size(149, 30);
            this.labelMaxSockets.TabIndex = 43;
            this.labelMaxSockets.Text = "Max sockets";
            this.labelMaxSockets.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // textBoxMaxSlots
            // 
            this.textBoxMaxSlots.Location = new System.Drawing.Point(420, 274);
            this.textBoxMaxSlots.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxMaxSlots.Name = "textBoxMaxSlots";
            this.textBoxMaxSlots.Size = new System.Drawing.Size(132, 22);
            this.textBoxMaxSlots.TabIndex = 44;
            this.textBoxMaxSlots.TextChanged += new System.EventHandler(this.textBoxMaxSlots_TextChanged);
            // 
            // textBoxCashCheck
            // 
            this.textBoxCashCheck.Location = new System.Drawing.Point(420, 244);
            this.textBoxCashCheck.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxCashCheck.Name = "textBoxCashCheck";
            this.textBoxCashCheck.Size = new System.Drawing.Size(132, 22);
            this.textBoxCashCheck.TabIndex = 45;
            // 
            // tabBasicInfo
            // 
            this.tabBasicInfo.Controls.Add(this.tableLayoutPanel4);
            this.tabBasicInfo.Location = new System.Drawing.Point(4, 25);
            this.tabBasicInfo.Margin = new System.Windows.Forms.Padding(4);
            this.tabBasicInfo.Name = "tabBasicInfo";
            this.tabBasicInfo.Padding = new System.Windows.Forms.Padding(4);
            this.tabBasicInfo.Size = new System.Drawing.Size(849, 465);
            this.tabBasicInfo.TabIndex = 2;
            this.tabBasicInfo.Text = "Basic";
            this.tabBasicInfo.UseVisualStyleBackColor = true;
            this.tabBasicInfo.Click += new System.EventHandler(this.tabBasicInfo_Click);
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.ColumnCount = 4;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel4.Controls.Add(this.labelName, 0, 0);
            this.tableLayoutPanel4.Controls.Add(this.labelWeaponType, 0, 3);
            this.tableLayoutPanel4.Controls.Add(this.textBoxName, 1, 0);
            this.tableLayoutPanel4.Controls.Add(this.comboBoxWeaponType, 1, 3);
            this.tableLayoutPanel4.Controls.Add(this.comboBoxWeaponThirdType, 1, 4);
            this.tableLayoutPanel4.Controls.Add(this.label1, 0, 4);
            this.tableLayoutPanel4.Controls.Add(this.buttonLoadIcon, 1, 8);
            this.tableLayoutPanel4.Controls.Add(this.labelIco, 2, 8);
            this.tableLayoutPanel4.Controls.Add(this.labelXSDindex, 2, 0);
            this.tableLayoutPanel4.Controls.Add(this.textBoxXSDindex, 3, 0);
            this.tableLayoutPanel4.Controls.Add(this.label3, 0, 1);
            this.tableLayoutPanel4.Controls.Add(this.label4, 0, 2);
            this.tableLayoutPanel4.Controls.Add(this.comboBoxNickname, 1, 1);
            this.tableLayoutPanel4.Controls.Add(this.comboBoxAddTo, 1, 2);
            this.tableLayoutPanel4.Controls.Add(this.buttonCopy, 3, 8);
            this.tableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel4.Location = new System.Drawing.Point(4, 4);
            this.tableLayoutPanel4.Margin = new System.Windows.Forms.Padding(4);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 9;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel4.Size = new System.Drawing.Size(841, 457);
            this.tableLayoutPanel4.TabIndex = 0;
            // 
            // labelName
            // 
            this.labelName.AutoSize = true;
            this.labelName.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelName.Location = new System.Drawing.Point(4, 0);
            this.labelName.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelName.Name = "labelName";
            this.labelName.Size = new System.Drawing.Size(131, 30);
            this.labelName.TabIndex = 0;
            this.labelName.Text = "Name";
            this.labelName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelWeaponType
            // 
            this.labelWeaponType.AutoSize = true;
            this.labelWeaponType.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelWeaponType.Location = new System.Drawing.Point(4, 94);
            this.labelWeaponType.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelWeaponType.Name = "labelWeaponType";
            this.labelWeaponType.Size = new System.Drawing.Size(131, 32);
            this.labelWeaponType.TabIndex = 1;
            this.labelWeaponType.Text = "Weapon Main Type";
            this.labelWeaponType.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // textBoxName
            // 
            this.textBoxName.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBoxName.Location = new System.Drawing.Point(143, 4);
            this.textBoxName.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxName.Name = "textBoxName";
            this.textBoxName.ReadOnly = true;
            this.textBoxName.Size = new System.Drawing.Size(160, 22);
            this.textBoxName.TabIndex = 2;
            // 
            // comboBoxWeaponType
            // 
            this.comboBoxWeaponType.Dock = System.Windows.Forms.DockStyle.Fill;
            this.comboBoxWeaponType.FormattingEnabled = true;
            this.comboBoxWeaponType.Items.AddRange(new object[] {
            "SHORT",
            "LONG",
            "SOFT",
            "HIDDEN",
            "MUSICAL",
            "SPECIAL"});
            this.comboBoxWeaponType.Location = new System.Drawing.Point(143, 98);
            this.comboBoxWeaponType.Margin = new System.Windows.Forms.Padding(4);
            this.comboBoxWeaponType.Name = "comboBoxWeaponType";
            this.comboBoxWeaponType.Size = new System.Drawing.Size(160, 24);
            this.comboBoxWeaponType.TabIndex = 3;
            // 
            // comboBoxWeaponThirdType
            // 
            this.comboBoxWeaponThirdType.Dock = System.Windows.Forms.DockStyle.Fill;
            this.comboBoxWeaponThirdType.FormattingEnabled = true;
            this.comboBoxWeaponThirdType.Location = new System.Drawing.Point(143, 130);
            this.comboBoxWeaponThirdType.Margin = new System.Windows.Forms.Padding(4);
            this.comboBoxWeaponThirdType.Name = "comboBoxWeaponThirdType";
            this.comboBoxWeaponThirdType.Size = new System.Drawing.Size(160, 24);
            this.comboBoxWeaponThirdType.TabIndex = 4;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Location = new System.Drawing.Point(4, 126);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(131, 32);
            this.label1.TabIndex = 5;
            this.label1.Text = "Weapon Type";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // buttonLoadIcon
            // 
            this.buttonLoadIcon.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonLoadIcon.Location = new System.Drawing.Point(143, 162);
            this.buttonLoadIcon.Margin = new System.Windows.Forms.Padding(4);
            this.buttonLoadIcon.Name = "buttonLoadIcon";
            this.buttonLoadIcon.Size = new System.Drawing.Size(85, 39);
            this.buttonLoadIcon.TabIndex = 6;
            this.buttonLoadIcon.Text = "Load icon";
            this.buttonLoadIcon.UseVisualStyleBackColor = true;
            // 
            // labelIco
            // 
            this.labelIco.AutoSize = true;
            this.labelIco.Location = new System.Drawing.Point(311, 158);
            this.labelIco.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelIco.Name = "labelIco";
            this.labelIco.Size = new System.Drawing.Size(0, 17);
            this.labelIco.TabIndex = 7;
            // 
            // labelXSDindex
            // 
            this.labelXSDindex.AutoSize = true;
            this.labelXSDindex.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelXSDindex.Location = new System.Drawing.Point(311, 0);
            this.labelXSDindex.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelXSDindex.Name = "labelXSDindex";
            this.labelXSDindex.Size = new System.Drawing.Size(73, 30);
            this.labelXSDindex.TabIndex = 8;
            this.labelXSDindex.Text = "XSD index";
            this.labelXSDindex.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // textBoxXSDindex
            // 
            this.textBoxXSDindex.Location = new System.Drawing.Point(392, 4);
            this.textBoxXSDindex.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxXSDindex.Name = "textBoxXSDindex";
            this.textBoxXSDindex.Size = new System.Drawing.Size(132, 22);
            this.textBoxXSDindex.TabIndex = 9;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label3.Location = new System.Drawing.Point(4, 30);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(131, 32);
            this.label3.TabIndex = 10;
            this.label3.Text = "Nickname";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label4.Location = new System.Drawing.Point(4, 62);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(131, 32);
            this.label4.TabIndex = 11;
            this.label4.Text = "Add to";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // comboBoxNickname
            // 
            this.comboBoxNickname.FormattingEnabled = true;
            this.comboBoxNickname.Location = new System.Drawing.Point(143, 34);
            this.comboBoxNickname.Margin = new System.Windows.Forms.Padding(4);
            this.comboBoxNickname.Name = "comboBoxNickname";
            this.comboBoxNickname.Size = new System.Drawing.Size(160, 24);
            this.comboBoxNickname.TabIndex = 12;
            // 
            // comboBoxAddTo
            // 
            this.comboBoxAddTo.FormattingEnabled = true;
            this.comboBoxAddTo.Location = new System.Drawing.Point(143, 66);
            this.comboBoxAddTo.Margin = new System.Windows.Forms.Padding(4);
            this.comboBoxAddTo.Name = "comboBoxAddTo";
            this.comboBoxAddTo.Size = new System.Drawing.Size(160, 24);
            this.comboBoxAddTo.TabIndex = 13;
            // 
            // buttonCopy
            // 
            this.buttonCopy.Location = new System.Drawing.Point(392, 162);
            this.buttonCopy.Margin = new System.Windows.Forms.Padding(4);
            this.buttonCopy.Name = "buttonCopy";
            this.buttonCopy.Size = new System.Drawing.Size(163, 28);
            this.buttonCopy.TabIndex = 14;
            this.buttonCopy.Text = "Copy to new weapon";
            this.buttonCopy.UseVisualStyleBackColor = true;
            this.buttonCopy.Click += new System.EventHandler(this.buttonCopy_Click);
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.tabControl1.Controls.Add(this.tabBasicInfo);
            this.tabControl1.Controls.Add(this.tabDetails);
            this.tabControl1.Controls.Add(this.tabRequirements);
            this.tabControl1.Controls.Add(this.tabPageEffects);
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPageUnknownBytes);
            this.tabControl1.Location = new System.Drawing.Point(346, 48);
            this.tabControl1.Margin = new System.Windows.Forms.Padding(4);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(857, 494);
            this.tabControl1.TabIndex = 2;
            // 
            // FormWeapons
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1237, 592);
            this.Controls.Add(this.listBoxWeap);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.comboBoxWep);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormWeapons";
            this.ShowIcon = false;
            this.Text = "Weapon Editor";
            this.Load += new System.EventHandler(this.FormWeapons_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.FormWeapons_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.effectBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.itemWeaponBindingSource)).EndInit();
            this.tabPage2.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.tabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.tabPageEffects.ResumeLayout(false);
            this.tabPageUnknownBytes.ResumeLayout(false);
            this.tabRequirements.ResumeLayout(false);
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            this.tabDetails.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.tabBasicInfo.ResumeLayout(false);
            this.tableLayoutPanel4.ResumeLayout(false);
            this.tableLayoutPanel4.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.ComboBox comboBoxWep;
        private System.Windows.Forms.BindingSource effectBindingSource;
        private System.ComponentModel.Design.BinaryEditor be = new System.ComponentModel.Design.BinaryEditor();
        private System.Windows.Forms.BindingSource itemWeaponBindingSource;
        private System.Windows.Forms.ListBox listBoxWeap;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.CheckBox checkBoxCantNPC;
        private System.Windows.Forms.CheckBox checkBoxCantTrade;
        private System.Windows.Forms.CheckBox checkBoxCantStorage;
        private System.Windows.Forms.CheckBox checkBoxStatsTrade;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridViewTextBoxColumn iDDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn nameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn descriptionDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn valueDataGridViewTextBoxColumn;
        private System.Windows.Forms.TabPage tabPageEffects;
        private System.Windows.Forms.ListBox listBoxEffects;
        private System.Windows.Forms.TabPage tabPageUnknownBytes;
        private System.ComponentModel.Design.ByteViewer byteViewer;
        private System.Windows.Forms.TabPage tabRequirements;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.Label labelMainStat;
        private System.Windows.Forms.Label labelSecondaryStat;
        private System.Windows.Forms.TextBox textBoxSecondaryStat;
        private System.Windows.Forms.TextBox textBoxMainStat;
        private System.Windows.Forms.Label labelLevel;
        private System.Windows.Forms.TextBox textBoxLevel;
        private System.Windows.Forms.Label labelTextLevel;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TabPage tabDetails;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Label labelMinDmg;
        private System.Windows.Forms.Label labelMaxDmg;
        private System.Windows.Forms.Label labelDurability;
        private System.Windows.Forms.Label labelBalance;
        private System.Windows.Forms.Label labelAR;
        private System.Windows.Forms.Label labelCrit;
        private System.Windows.Forms.TextBox textBoxMinDmg;
        private System.Windows.Forms.TextBox textBoxMaxDmg;
        private System.Windows.Forms.TextBox textBoxDurability;
        private System.Windows.Forms.TextBox textBoxBalance;
        private System.Windows.Forms.TextBox textBoxAR;
        private System.Windows.Forms.TextBox textBoxCrit;
        private System.Windows.Forms.TextBox textBoxHardness;
        private System.Windows.Forms.Label labelHardness;
        private System.Windows.Forms.Label labelTears;
        private System.Windows.Forms.TextBox textBoxTears;
        private System.Windows.Forms.Label labelPrice;
        private System.Windows.Forms.TextBox textBoxPrice;
        private System.Windows.Forms.Label labelSockets;
        private System.Windows.Forms.TextBox textBoxSockets;
        private System.Windows.Forms.Label labelRank;
        private System.Windows.Forms.TextBox textBoxRank;
        private System.Windows.Forms.Label labelModel;
        private System.Windows.Forms.Label labelIcon;
        private System.Windows.Forms.TextBox textBoxModel;
        private System.Windows.Forms.TextBox textBoxIcon;
        private System.Windows.Forms.Label labelTime;
        private System.Windows.Forms.TextBox textBoxTime;
        private System.Windows.Forms.Label labelContribClan;
        private System.Windows.Forms.Label labelContribOthers;
        private System.Windows.Forms.TextBox textBoxContribClan;
        private System.Windows.Forms.TextBox textBoxContribOthers;
        private System.Windows.Forms.Label labelLvlRed;
        private System.Windows.Forms.TextBox textBoxLvlRed;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label labelCashCheck;
        private System.Windows.Forms.Label labelMaxSockets;
        private System.Windows.Forms.TextBox textBoxMaxSlots;
        private System.Windows.Forms.TextBox textBoxCashCheck;
        private System.Windows.Forms.TabPage tabBasicInfo;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private System.Windows.Forms.Label labelName;
        private System.Windows.Forms.Label labelWeaponType;
        private System.Windows.Forms.TextBox textBoxName;
        private System.Windows.Forms.ComboBox comboBoxWeaponType;
        private System.Windows.Forms.ComboBox comboBoxWeaponThirdType;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button buttonLoadIcon;
        private System.Windows.Forms.Label labelIco;
        private System.Windows.Forms.Label labelXSDindex;
        private System.Windows.Forms.TextBox textBoxXSDindex;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox comboBoxNickname;
        private System.Windows.Forms.ComboBox comboBoxAddTo;
        private System.Windows.Forms.Button buttonCopy;
        private System.Windows.Forms.TabControl tabControl1;
    }
}

